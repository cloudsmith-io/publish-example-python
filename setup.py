"""Cloudsmith Example Package.
"""
from __future__ import absolute_import, print_function

from setuptools import setup


setup(
    name="cloudsmith",
    url="https://cloudsmith.io",
    license="Apache License 2.0",
    author="Cloudsmith Ltd",
    author_email="support@cloudsmith.io",
    description="Cloudsmith: Be Awesome. Automate Everything.",
    py_modules=["cloudsmith"],
    entry_points={
        "console_scripts": [
            "cloudsmith=cloudsmith:be_awesome"
        ]
    },
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    keywords=["cloudsmith", "python", "bitbucket", "example"],
)
